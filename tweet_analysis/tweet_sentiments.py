
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from textblob import TextBlob
from tweet_collection import twitter_users
from tweet_collection.to_dataframe import transform_to_dataframe
from tweet_collection.tweet_collect import collect_tweets
from tweet_collection.twitter_users import collect_by_user

##
data = transform_to_dataframe(twitter_users.collect_by_user(1976143068,100))


def goodness(text):#à affiner ?
   s = TextBlob(text)
   try:
       s = s.translate(to='en')
   except:
       s = s
   return (s).sentiment.polarity

def polarity_list(data):
   for t in data:
       print (t,goodness(t))

def polarity_lists(data):
   list_pos=[]
   list_neu=[]
   list_neg=[]
   for tweet in data:
       g=goodness(tweet)
       if g>0:
           list_pos.append(tweet)
       elif g<0:
           list_neg.append(tweet)
       else:
           list_neu.append(tweet)
   return (list_pos,list_neu,list_neg)

def percentage(pos,neu,neg):
   n=pos+neu+neg
   print("Percentage of positive tweets: {}%".format(100*pos/n))
   print("Percentage of neutral tweets: {}%".format(100*neu/n))
   print("Percentage de negative tweets: {}%".format(100*neg/n))

def merge(L,M):
   K=[i for i in L]
   for i in M:
       if i not in K:
           K.append(i)
   return K

##
a,b,c=polarity_lists(data['tweet_textual_content'])
percentage(len(a),len(b),len(c))
##la fonction finale

def analyse_popularite(candidat,iterations,n):
    """

    :param candidat: le ID du compte twitter voulu.
    :param iterations: Nombre de fois qu'on effectue une collection de tweets (On a remarqué que si on l'effectue plusieurs fois on récupère plus que 100 tweets)
    :param n: Nombre de tweets a collecté. Max 100
    :return: pourcentage des opinions positives, négatives et neutres.
    """
    pos,neu,neg=[],[],[]
    for i in range(iterations):
           data=transform_to_dataframe(collect_by_user(candidat,n))
           p,m,n=polarity_lists(data["tweet_textual_content"])
           pos = merge(pos,p)
           neu = merge(neu,m)
           neg = merge(neg,n)
           print("done: {}%".format(100*(i+1)/iterations))
       percentage(len(pos),len(neu),len(neg))
    return len(pos),len(neg)


