import numpy as np
import pandas as pd
import twitterPredictor
from tweet_collection import twitter_users
from tweet_collection import to_dataframe

def tweet_likes(user_id,nb_tweet):
    '''
        :param user_id: le ID du compte twitter voulu.
        :param nb_tweet: nombre de tweets a collecté, max 100.
        :return: affiche le tweet avec le plus des likes
    '''
    data = to_dataframe.transform_to_dataframe(twitter_users.collect_by_user(user_id,nb_tweet))
    likes_max  = np.max(data['likes'])
    likes  = data[data.likes == likes_max].index[0]

    # Max RTs:
    print("The tweet with more likes is: \n{}".format(data['tweet_textual_content'][likes]))
    print("Number of likes: {}".format(likes_max))
    print("{} characters.\n".format(data['len'][likes]))
