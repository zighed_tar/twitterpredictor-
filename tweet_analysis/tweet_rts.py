import numpy as np
import pandas as pd
import twitterPredictor
from tweet_collection import twitter_users
from tweet_collection import to_dataframe

def tweet_rts(user_id,nb_tweet):
    '''
        :param user_id: le ID du compte twitter voulu.
        :param nb_tweet: nombre de tweets a collecté, max 100.
        :return: affiche le tweet avec le plus de retweets
    '''
    data = to_dataframe.transform_to_dataframe(twitter_users.collect_by_user(user_id,nb_tweet))
    rt_max  = np.max(data['RTs'])
    rt  = data[data.RTs == rt_max].index[0]

    # Max RTs:
    print("The tweet with more retweets is: \n{}".format(data['tweet_textual_content'][rt]))
    print("Number of retweets: {}".format(rt_max))
    print("{} characters.\n".format(data['len'][rt]))
