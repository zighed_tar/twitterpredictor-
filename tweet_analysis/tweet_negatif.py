import numpy as np
import pandas as pd
import twitterPredictor
from tweet_collection import twitter_users
from tweet_collection import to_dataframe

def tweet_negatif(user_id,nb_tweet):
    '''
        :param user_id: le ID du compte twitter voulu.
        :param nb_tweet: nombre de tweets a collecté, max 100.
        :return: affiche le nombre des mots négatifs dans les tweets collectés.
    '''

    data = to_dataframe.transform_to_dataframe(twitter_users.collect_by_user(user_id,nb_tweet))

    list_pos = ["loose","bad","suck","mauvais","idiot"]
    tweet_text = data["tweet_textual_content"]
    c=0
    for mot in list_pos:
        for text in tweet_text:
            if mot in str(text):
                c+=1
                print("**",text)
    print("le nombre de mot négatif dans les tweets {}".format(c))
    print(len(tweet_text))
