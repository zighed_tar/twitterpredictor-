import numpy as np
import pandas as pd
import twitterPredictor
from tweet_collection import twitter_users
from tweet_collection import to_dataframe
from textblob import TextBlob
from textblob import Word


def tweet_textblob(user_id,nb_tweets):
    '''

    :param user_id: le ID du compte twitter voulu.
    :param nb_tweets: Nombre de tweets a collecté. Max 100
    :return: cette fonction lemmatise les tweets collectés.
    '''

    data = to_dataframe.transform_to_dataframe(twitter_users.collect_by_user(user_id,nb_tweets))

    for tweet in data["tweet_textual_content"]:
        tweetblob = TextBlob(tweet)
        tw= tweetblob.words
        for words in tw:
            tw.lemmatize()


