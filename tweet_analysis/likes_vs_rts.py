import numpy as np
import pandas as pd
from tweet_collection import twitter_users
from tweet_collection import to_dataframe
import matplotlib.pyplot as plt

def likes_vs_rts(user_id,nb_tweet):
    '''
    :param user_id: le ID du compte twitter voulu.
    :param nb_tweet: nombre de tweets a collecté, max 100.
    :return: affiche un graph nombre de likes et nombre de retweets en fonction de la date
    '''
    data = to_dataframe.transform_to_dataframe(twitter_users.collect_by_user(user_id,nb_tweet))

    tfav = pd.Series(data=data['likes'].values, index=data['date'])
    tret = pd.Series(data=data['RTs'].values, index=data['date'])

    # Likes vs retweets visualization:
    tfav.plot(figsize=(16,4), label="Likes", legend=True)
    tret.plot(figsize=(16,4), label="Retweets", legend=True)


    plt.show()

