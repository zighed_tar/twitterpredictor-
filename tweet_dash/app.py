import dash
import dash_core_components as dcc
import dash_html_components as html
from tweet_analysis import tweet_sentiments


def tweet_dash(user_id1,user_id2,itér,nb_tweets):
    """
        Fonction qui affiche le pourcentage des tweets positifs et négative de deux compte twitter
        :param user_id1: ID du premier compte twitter voulu.
        :param user_id2: ID du deuxième compte twitter voulu.
        :param itér: Nombre de fois qu'on appellera la fonction qui collectera les tweets ( On a remarqué qu'on peut dépasser la limite de 100 tweets si on appelle plusieurs fois la fonction qui collecte les tweets.
        :param nb_tweets: Nimbre de tweet a collecté. Max 100
        :return: Un site web avec d'un graph intéractif avec le pourcentage des tweets positifs et négative de deux compte twitter
        """


    ListMacron = tweet_sentiments.analyse_popularite(user_id1,itér,nb_tweets)#Macron 1976143068
    ListLePen = tweet_sentiments.analyse_popularite(user_id2,itér,nb_tweets)#LePen 80378916

    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

    colors = {
        'background': '#111111',
        'text': '#7FDBFF'
    }

    app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
        html.H1(
            children='Political Opinion',
            style={
                'textAlign': 'center',
                'color': colors['text']
            }
        ),

        html.Div(children='Macron vs LePen : Opinion générale', style={
            'textAlign': 'center',
            'color': colors['text']
        }),

        dcc.Graph(
            id='example-graph-2',
            figure={
                'data': [
                    {'x': ["Macron", "LePen"], 'y': [ListMacron[0],ListLePen[0]], 'type': 'bar', 'name': 'Positive'},
                    {'x': ["Macron", "LePen"], 'y': [ListMacron[1],ListLePen[1]], 'type': 'bar', 'name': u'Negative'},
                ],
                'layout': {
                    'plot_bgcolor': colors['background'],
                    'paper_bgcolor': colors['background'],
                    'font': {
                        'color': colors['text']
                    }
                }
            }
        )
    ])

    if __name__ == '__main__':
        app.run_server(debug=True)