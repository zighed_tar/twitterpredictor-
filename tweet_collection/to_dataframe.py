import pandas as pd
import twitterPredictor
from tweet_collection import twitter_users
from datetime import datetime, timezone
import pytz


def transform_to_dict(tweet):
    """

    :param tweet: le tweet collecté.
    :return: un dictionnaire des diffetentes données relatives au tweet.
    """

    return {"tweet_textual_content": str(tweet.text),
            "len" : len(tweet.text),
            "likes": tweet.favorite_count,
            "RTs": tweet.retweet_count,
            "date": tweet.created_at.isoformat()}


def transform_to_dataframe(tweets):
    """

    :param tweets: le tweet collecté.
    :return: un dataframe avec le dictionnaire des diffetentes données relatives au tweet.
    """
    dict_list = []
    for tweet in tweets:
        dict_list.append(transform_to_dict(tweet))
    data_frame = pd.DataFrame(dict_list)
    return data_frame


