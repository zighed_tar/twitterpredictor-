import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from textblob import TextBlob
from tweet_collection import twitter_users
from tweet_collection.to_dataframe import transform_to_dataframe
from tweet_collection.tweet_collect import collect_tweets
from tweet_analysis import tweet_sentiments
import seaborn as sns

def tweet_afficher(user_id1,user_id2,itér,nb_tweets):
    """
    Fonction qui affiche le pourcentage des tweets positifs et négative de deux compte twitter
    :param user_id1: ID du premier compte twitter voulu.
    :param user_id2: ID du deuxième compte twitter voulu.
    :param itér: Nombre de fois qu'on appellera la fonction qui collectera les tweets ( On a remarqué qu'on peut dépasser la limite de 100 tweets si on appelle plusieurs fois la fonction qui collecte les tweets.
    :param nb_tweets: Nimbre de tweet a collecté. Max 100
    :return: un graph avec le pourcentage des tweets positifs et négative de deux compte twitter
    """

    pos1,neg1=tweet_sentiments.analyse_popularite(user_id1,itér,nb_tweets)##Macron 1976143068
    pos2,neg2=tweet_sentiments.analyse_popularite(user_id2,itér,nb_tweets)##LePen 80378916

    sns.set(style="white", context="talk")
    rs = np.random.RandomState(8)

    f, (ax1, ax2) = plt.subplots(2, 1, figsize=(7, 5), sharex=True)
    x = ["Macron","LePen"]
    y1 = [pos1,pos2]
    sns.barplot(x=x, y=y1, palette="rocket", ax=ax1)
    ax1.axhline(0, color="k", clip_on=False)
    ax1.set_ylabel("Postive Opinion")

    y2 = [neg1,neg2]
    sns.barplot(x=x, y=y2, palette="vlag", ax=ax2)
    ax2.axhline(0, color="k", clip_on=False)
    ax2.set_ylabel("Negative Opinion")

    sns.despine(bottom=True)
    plt.setp(f.axes, yticks=[])
    plt.tight_layout(h_pad=2)

    plt.show()

