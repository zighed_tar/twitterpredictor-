from tweet_collection.twitter_search import collect
from tweet_collection.to_dataframe import transform_to_dataframe
from pytest import *

def test_collect():
    """
    :return: on teste la fonction collect et dataframe.
    """
    search_term = 'EmmanuelMacron'
    tweets = collect(search_term)
    data = transform_to_dataframe(tweets)
    # print(data[data.columns[2]])
    assert 'tweet_textual_content' in data.columns
