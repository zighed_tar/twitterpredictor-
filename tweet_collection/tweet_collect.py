from tweet_collection.twitter_connection_setup import twitter_setup

def collect_tweets(input,n):
   """

   :param input: ID du compte twitter voulu.
   :param n: nombre de tweet à collecter. Max 100
   :return: collection de tweets
   """
   connexion = twitter_setup()
   tweets = connexion.search(input,lng="en",rpp=1000,count=n)
   return tweets