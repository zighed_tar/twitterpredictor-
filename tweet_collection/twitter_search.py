from tweet_collection.twitter_connection_setup import twitter_setup

def collect(search_term):
    """
    The function takes a search term and returns the related tweets
    """
    print('Collecting the tweets related to {}'.format(search_term))
    connexion = twitter_setup()
    tweets = connexion.search("Emmanuel Macron", lng="en", rpp=100,count =100)
    return tweets

