from tweet_collection.twitter_connection_setup import twitter_setup

def collect_by_user(user_id,n):
    """
    The function takes a user_id and returns its status
    """
    connexion = twitter_setup()
    username = connexion.get_user(user_id).screen_name
    print('Collecting the statuses of the username {} with ID {}'.format(username, user_id))
    statuses = connexion.user_timeline(id=user_id, count=n)
    for status in statuses:
        print(status.text)
    return statuses


