from tweet_collection.twitter_connection_setup import twitter_setup

def get_retweets_of_candidate(candidate_username):
    """

    :param candidate_username: Username du compte twitter voulu.
    :return: les retweets.
    """
    connection = twitter_setup()
    tweets = connection.user_timeline(screen_name=candidate_username, count=200)
    retweets_per_tweet = {}
    for status in tweets:
        # Only for original tweets, excluding the retweets
        if (not status.retweeted) or ('RT @' not in status.text):
            retweets_per_tweet[status.text] = status.retweet_count
    return retweets_per_tweet


############## Testing #################
if __name__ == "__main__": # If we execute this file
    username = 'EmmanuelMacron'
    retweets_per_tweet = get_retweets_of_candidate('EmmanuelMacron')
    for tweet, nb_rt in retweets_per_tweet.items():
        print('{}, the number of retweets are --> {}'.format(tweet, nb_rt))