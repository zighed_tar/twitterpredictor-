from tweet_collection.twitter_connection_setup import twitter_setup
from tweepy.streaming import StreamListener
import tweepy

class StdOutListener(StreamListener):
    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if str(status) == "420":
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True

def collect_by_streaming(term_to_track):
    connexion = twitter_setup()
    listener = StdOutListener()
    stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    stream.filter(track=[term_to_track])
