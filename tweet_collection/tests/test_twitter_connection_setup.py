import pytest
from tweet_collection.twitter_connection_setup import twitter_setup

def test_twitter_setup():
    """
    Testing the functionality of the twitter_setup
    """
    connection = twitter_setup()
    assert connection.me() # exists only if we have an authentification
    # To test please run in the terminal: python -m pytest