from tweet_collection import twitter_users
from tweet_collection import to_dataframe
from textblob import TextBlob
import goslate





def textTweet(user_id,nb_tweets):
   '''
       :param user_id: le ID du compte twitter voulu.
       :param nb_tweet: nombre de tweets a collecté, max 100.
       :return: Un string de tout les tweets collectés.
   '''
   data = to_dataframe.transform_to_dataframe(twitter_users.collect_by_user(user_id,nb_tweets))
   text=''
   for i in data['tweet_textual_content']:
      s=TextBlob(" "+i)
      text += str(s)

   return text


