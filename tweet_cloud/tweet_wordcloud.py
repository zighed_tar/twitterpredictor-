import multidict as multidict
import nltk
import numpy as np
from nltk.corpus import stopwords
import os
import re
from PIL import Image
from os import path
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from tweet_cloud.tweet_words import textTweet
from textblob import TextBlob
import goslate
gs = goslate.Goslate()

def unmaj(text):
    '''
    :param text: le text des tweets.
    :return: le text sans majuscule.
    '''
    s=''
    for i in text:
           if ord(i)>=ord('A') and ord(i)<=ord('Z'):
               s += chr(ord(i)+ord('a')-ord('A'))
           else:
               s += i
    return s

def getFrequencyDictForText(sentence):
    """

    :param sentence: Un string du texte de tout les tweets collectés
    :return: dictionnaire avec les mots les plus fréquents
    """


    fullTermsDict = multidict.MultiDict()
    tmpDict = {}

    # making dict for counting frequencies
    for text in re.findall(r"[\w']+", sentence):
        if unmaj(text) in stopwords.words('french') or unmaj(text) in ['http','https','être','avoir','a','les','co'] or unmaj(text) in stopwords.words('english'):
            continue
        val = tmpDict.get(text, 0)
        tmpDict[text.lower()] = val + 1
    for key in tmpDict:
           fullTermsDict.add(key, tmpDict[key])
    return fullTermsDict


def makeImage(text):
    """
    :param text:  Un string du texte de tout les tweets collectés
    :return: une image avec un mask de macron de tout les mots fréquents dit sur lui
    """

    alice_mask = np.array(Image.open("macron.jpg"))
    wc = WordCloud(background_color="white", max_words=1000, mask=alice_mask)
    # generate word cloud
    wc.generate_from_frequencies(text)

    # show
    plt.imshow(wc, interpolation="bilinear")
    plt.axis("off")
    plt.show()


makeImage(getFrequencyDictForText(textTweet(user_id,nb_tweets)))##
##################
